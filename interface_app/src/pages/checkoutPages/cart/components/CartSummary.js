import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(6),
    padding: theme.spacing(2),
    position: "-webkit-sticky",
    position: "sticky",
    top: theme.spacing(1)
  },
  right: {
    float: "right"
  },
  divider: {
    margin: theme.spacing(1)
  },
  control: {
    marginBottom: theme.spacing(2)
  }
}));

const CartSummary = ({ cart }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <div className={classes.control}>
        <Typography display="inline">
          Общая стоимость 
          {/* ({cart.items_count} товаров в корщине) */}
        </Typography>
        <Typography display="inline" className={classes.right}>
          {(cart.total_price + cart.total_customer_profit).toFixed(2)}р.
        </Typography>
      </div>
      <div className={classes.control}>
        <Typography color="primary" display="inline">
          Скидка составляет:
        </Typography>
        <Typography color="primary" display="inline" className={classes.right}>
          {cart.total_customer_profit}р.
        </Typography>
      </div>
      <div className={classes.control}>
        <Typography display="inline">Финальная стоимость:</Typography>
        <Typography display="inline" className={classes.right}>
          {cart.total_price}р.
        </Typography>
      </div>
      <Divider className={classes.divider} />
      <div>
        <Typography variant="h6" align="center">
          К оплате:
        </Typography>
        <Typography variant="h6" align="center">
          {cart.total_price}р.
        </Typography>
      </div>
    </Paper>
  );
};

export default CartSummary;
