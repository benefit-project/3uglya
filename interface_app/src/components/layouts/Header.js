import React, { useEffect } from "react";
import { Link as RouterLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import AppBar from "@material-ui/core/AppBar";
import Badge from "@material-ui/core/Badge";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import IconButton from "@material-ui/core/IconButton";
import CartIcon from "@material-ui/icons/ShoppingCart";
import ProfileIcon from "@material-ui/icons/Person";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Search from "@pages/productPages/products/components/Filters/Search";
import { Fade, Paper } from "@material-ui/core";
import Collapse from '@material-ui/core/Collapse';
import { fetchMainCategories } from "@actions/categoryActions";

const useStyles = makeStyles((theme) => ({
  rightItems: {
    marginLeft: "auto",
    display: "flex",
  },
  shopButton: {
    marginLeft: theme.spacing(1),
  },
  title: {
    marginRight: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(0.5),
  },
  polygon: {
    fill: theme.palette.common.white,
    stroke: theme.palette.divider,
    strokeWidth: 1,
  },
}));

const Header = () => {
  const dispatch = useDispatch();
  const categories = useSelector(state => state);
  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  useEffect(() => {
    dispatch(fetchMainCategories());
    console.log("categories:", categories)
  }, []);

  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    console.log("categories:", categories)
    
  }

  function handleClose() {
    setAnchorEl(null);
  }

  const classes = useStyles();
  const { isAuthenticated, user } = useSelector((state) => state.auth);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down("xs"));

  const authNav = (
    <div className={classes.rightItems}>
      <IconButton component={RouterLink} to="/profile" color="inherit">
        <ProfileIcon />
      </IconButton>
      <IconButton component={RouterLink} to="/cart" color="inherit">
        <Badge color="secondary" badgeContent={user && user.cart_items_count}>
          <CartIcon />
        </Badge>
      </IconButton>
    </div>
  );

  const guestNav = (
    <div className={classes.rightItems}>
      <Button component={RouterLink} to="/login" color="inherit">
        Войти
      </Button>
      <Button component={RouterLink} to="/register" color="inherit">
        Регистрация
      </Button>
    </div>
  );

  return (
    <React.Fragment>
      <AppBar position="static">
        <Toolbar>
          <Link
            component={RouterLink}
            to="/products"
            variant="h4"
            color="inherit"
            className={classes.title}
            underline={"none"}
          >
            3&nbsp;угля
          </Link>
          <Button
            aria-owns={anchorEl ? "menu" : undefined}
            aria-haspopup="true"
            onClick={handleClick}
            // onMouseOver={handleClick}
          >
            Open Menu
          </Button>
          <Menu
            open={Boolean(anchorEl)}
            onClose={handleClose}
            MenuListProps={{ onMouseLeave: handleClose }}
          >
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <MenuItem onClick={handleClose}>Logout</MenuItem>
          </Menu>
          {!matches && <Search />}
          {isAuthenticated === false ? guestNav : authNav}
        </Toolbar>
        {/* <Toolbar>
          <Button
            size="small"
            component={Link}
            className={classes.button}
            color="secondary"
            onMouseOver={setChecked(true)}
          >
            Новые
          </Button>
          <Button
            size="small"
            component={Link}
            className={classes.button}
            color="secondary"
          >
            Сначала дорогие
          </Button>
          <Button
            size="small"
            component={Link}
            className={classes.button}
            color="secondary"
          >
            Сначала дешевые
          </Button>
          <Button
            size="small"
            component={Link}
            className={classes.button}
            color="secondary"
          >
            Популярные
          </Button>
        </Toolbar> */}
        {/* <Toolbar>
          <Fade in={checked}>
            <Paper elevation={4}>
              <svg className={classes.svg}>
                <polygon
                  points="0,100 50,00, 100,100"
                  className={classes.polygon}
                />
              </svg>
            </Paper>
          </Fade>
        </Toolbar> */}
      </AppBar>
      {matches && <Search />}
    </React.Fragment>
  );
};

export default Header;
