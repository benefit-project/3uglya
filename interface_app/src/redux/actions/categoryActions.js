import axios from "axios";

import {
  FETCH_MAIN_CATEGORIES,
  START_LOADING_UI,
  STOP_LOADING_UI
} from "../types";

export const fetchMainCategories = () => dispatch => {
  dispatch({ type: START_LOADING_UI });
  axios.get(`/api/categories/?main=true`).then(response => {
    dispatch({ type: FETCH_MAIN_CATEGORIES, payload: response.data });
    dispatch({ type: STOP_LOADING_UI });
  });
};

// export const fetchProduct = slug => dispatch => {
//   dispatch({ type: START_LOADING_UI });
//   axios.get(`/api/products/${slug}/`).then(response => {
//     dispatch({ type: FETCH_PRODUCT, payload: response.data });
//     dispatch({ type: STOP_LOADING_UI });
//   });
// };
