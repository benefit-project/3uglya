import {
  FETCH_MAIN_CATEGORIES
} from "../types";

const initialState = {
  main_categories: [],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_MAIN_CATEGORIES:
      console.log("FETCH_MAIN_CATEGORIES", payload)
      return { ...state,  main_categories: { ...payload }  };
    default:
      return state;
  }
};
