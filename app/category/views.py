from django.shortcuts import render

from rest_framework.generics import RetrieveAPIView, ListAPIView
from category.models import Category
from category.serializers import CategorySerializer


class CategoryListView(ListAPIView):
    # queryset = Category.objects.all()
    serializer_class = CategorySerializer
    # search_fields = ('title', 'description')
    # filterset_class = ProductFilter

    def get_queryset(self):
        queryset = Category.objects.all()
        # print(queryset)
        main = self.request.query_params.get('main', None)
        if main is not None and main == 'true':
            queryset = Category.objects.main_categories()
        return queryset


# class ProductDetailView(RetrieveAPIView):
#     queryset = Product.objects.all()
#     serializer_class = ProductDetailSerializer
#     lookup_field = 'slug'