from django.db import models
from autoslug import AutoSlugField

class CategoryManager(models.Manager):
    def all(self):
        return self.filter(parent__parent__parent__parent=None)

    def main_categories(self):
        return self.filter(parent=None)



class Category(models.Model):
    name = models.CharField(max_length=80)
    slug = AutoSlugField(populate_from='name',
                         unique_with=['name'], unique=True)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children', on_delete=models.DO_NOTHING)
    photo_main = models.ImageField(upload_to='brand_photos/%Y/%m/%d/', blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    objects = CategoryManager()

    def __str__(self):
        return self.name

class Brand(models.Model):
    name = models.CharField(max_length=80)
    slug = AutoSlugField(populate_from='name',
                         unique_with=['name'], unique=True)
    photo_main = models.ImageField(upload_to='brand_photos/%Y/%m/%d/', blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name
