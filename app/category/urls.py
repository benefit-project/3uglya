from django.urls import path

from . import views

urlpatterns = [
    path('', views.CategoryListView.as_view(), name='category-list'),
    # path('<slug>/', views.ProductDetailView.as_view(), name='product-detail'),
]
